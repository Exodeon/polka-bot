import os

from dotenv import load_dotenv

import discord
from discord.ext import commands

intents = discord.Intents.default()
intents.message_content = True
bot = commands.Bot(command_prefix="!", intents=intents)


@bot.event
async def on_ready():
    print('We have logged in as {0.user}'.format(bot))


# load config from dotenv
load_dotenv()

# roles
roles_message_id = int(os.getenv("ROLE_MESSAGE_ID"))
emoji_to_role = {"💿": "DJ",
                 "☎": "GarticPhone",
                 "🇯🇵": "Japonais",
                 "🔞": "Chaud Patate",
                 "lesmath": "AmongUs ඞ",
                 "🐀": "Parigo",
                 "🏭": "Rouen",
                 "🏝️": "Lille",
                 "🧊": "MiNeCrAfT",
                 "cutepika": "Pokémon Unite",
                 "🏁": "FallGy"}


@bot.event
async def on_raw_reaction_add(payload):
    if payload.message_id == roles_message_id:
        if payload.emoji.name in emoji_to_role:
            role = discord.utils.get(payload.member.guild.roles, name=emoji_to_role[payload.emoji.name])
            await payload.member.add_roles(role)


@bot.event
async def on_message(message):
    if message.author == bot.user:
        return

    await bot.process_commands(message)

# import commands
cog_files = ["google_drive_commands", "cat_command", "wordcloud_command", "emoji_command", "motivation_command",
             "pokefusion_command", "stats_command", "dico_command", "bored_command", "invert_command",
             "pokemon_command", "hour_command", "roll_command"]
commands_repository = "commands"
for cog_file in cog_files:
    bot.load_extension(commands_repository + "." + cog_file)
    print("%s has loaded." % cog_file)

try:
    token = os.getenv("BOT_DISCORD_TOKEN")
    bot.run(token)
except discord.errors.HTTPException as e:
    print(e)
    if e.status == 429:
        print("Time left before retry :", int(e.response.headers["Retry-After"]) / 60, "minutes")
