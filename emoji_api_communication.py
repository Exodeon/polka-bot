import requests

API_KEY = "69404831532ed5120ae3aab6c0458528f4225495"

API_BASE_URL = "https://emoji-api.com/"
API_END_URL = "?access_key=" + API_KEY


def get_all_emojis():
    response = requests.get(API_BASE_URL + "emojis" + API_END_URL)
    return response.json()


def get_emoji(name):
    response = requests.get(API_BASE_URL + "emojis/" + name + API_END_URL)
    return response.json()[0]["character"]


def get_categories():
    response = requests.get(API_BASE_URL + "categories" + API_END_URL)
    return response.json()


def get_emojis_in_category(category):
    response = requests.get(API_BASE_URL + "categories/" + category + API_END_URL)
    return [emoji["character"] for emoji in response.json()]


if __name__ == "__main__":
    print(get_emoji("grinning-face"))
    print(get_emojis_in_category("smileys-emotion"))
