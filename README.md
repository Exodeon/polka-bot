# Polka Bot
This is a Discord bot. You can find all commands in `commands/`.

## Getting started

1. Copy `.env.default` as `.env`, and fill the required fields.

2. If you want to use the `ImageCommand`, you need to add your `credentials.json` of Google Drive API, and run the bot once to generate a token.

3. You can now run `main.py`.
