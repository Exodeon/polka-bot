import discord
import requests
from discord.ext import commands

import pokeapi_communication


class PokemonCommand(commands.Cog):
    def __init__(self, client):
        self.client = client

    @commands.command(help="Give weakness of a Pokemon type\nPossible types are : " + str(pokeapi_communication.get_all_types()))
    async def pokeweakness(self, ctx, type: str):
        await ctx.message.channel.send(pokeapi_communication.get_weakness_for_type(type))


def setup(client):
    client.add_cog(PokemonCommand(client))
