import random
import dice

from discord.ext import commands


class RollCommand(commands.Cog):
    def __init__(self, client):
        self.client = client

    @commands.command(help="Roll a die, from 1 to max_value (included)")
    async def rollint(self, ctx, max_value: int):
        await ctx.message.channel.send(random.randint(1, max_value))

    @commands.command(help="Roll a die from a formula")
    async def roll(self, ctx, formula: str):
        roll_element = dice.roll(formula, raw=True)
        await ctx.message.channel.send(f"{roll_element.evaluate_cached()} ||= {''.join(dice.utilities.verbose_print(roll_element).splitlines())}||")


def setup(client):
    client.add_cog(RollCommand(client))
