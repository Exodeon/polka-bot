from discord.ext import commands
from re import match
from random import choice
from typing import Optional

from emoji_api_communication import *


# emoji
# emoji_category_mapper = {"custom": lambda: bot.emojis,
emoji_category_mapper = {"emotion": lambda: get_emojis_in_category("smileys-emotion"),
                         "body": lambda: get_emojis_in_category("people-body"),
                         "nature": lambda: get_emojis_in_category("animals-nature"),
                         "food": lambda: get_emojis_in_category("food-drink"),
                         "travel": lambda: get_emojis_in_category("travel-places"),
                         "activities": lambda: get_emojis_in_category("activities"),
                         "objects": lambda: get_emojis_in_category("objects"),
                         "symbols": lambda: get_emojis_in_category("symbols"),
                         "flags": lambda: get_emojis_in_category("flags")}


class EmojiCommand(commands.Cog):
    def __init__(self, client):
        self.client = client
        emoji_category_mapper["custom"] = lambda: client.emojis

    @commands.command(help="Show random emojis. You can specify categories after the emojis number : [category1, category2...]\n" +
                  "Categories are : " + str(list(emoji_category_mapper.keys())))
    async def emoji(self, ctx, emojis_number: Optional[int] = 1, *, arg=""):
        emojis = self.client.emojis
        matched = match(r"\[ *(\w* *, *)*\w+ *\]", arg)
        if matched:
            found_list = matched.group(0)
            categories = found_list[1:-1].split(",")
            categories = [category.strip() for category in categories]
            emojis = []
            for category in categories:
                if category in emoji_category_mapper:
                    emojis.extend(emoji_category_mapper[category]())
        message_to_send = ""
        for i in range(emojis_number):
            message_to_send += (str(choice(emojis)))
        await ctx.message.channel.send(message_to_send)


def setup(client):
    client.add_cog(EmojiCommand(client))
