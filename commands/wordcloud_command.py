import matplotlib.pyplot as plt
import os

import discord
from discord.ext import commands
from spacy.lang.fr.stop_words import STOP_WORDS as fr_stop
from wordcloud import WordCloud

# emojis added on ready
excluded_words = list(fr_stop) + \
                 ["tenor", "https", "com", "view", "www", "gif", "watch", "at", "e0", "a4", "fr", "issou"] + \
                 ["xd", "mdr", "tkt", "x'", "ptdr", "mdrrr", "mdrr"] + \
                 ["oui", "non", "yep", "nan", "nope", "bah", "j'ai", "j'en", "t'a", "t'as", "l'ai", "n'y", "d'une", "ouais", "t'es",
                  "d'être", "t'en", "m'a",
                  "rien", "faut", "mal", "coup", "c'est", "c'était", "bon", "x", "b", "c", "v", "d", "h", "m", "l", "j", "t", "ca",
                  "ok", "okay", "j'avais", "bien", "vraiment", "truc", "trucs", "fois", "temps", "crois", "veux", "vois",
                  "voir", "trouve", "sais", "met", "mettre", "n'est", "été", "qu'ils", "vrai", "trop", "d'un", "jamais",
                  "gros", "beaucoup", "ici", "genre", "hein", "moment", "veut", "voulez", "qu'il", "qu'on", "oh", "faire",
                  "fait", "ahah", "haha", "héhé"]


class WordcloudCommand(commands.Cog):
    def __init__(self, client):
        self.client = client

    @commands.command(help="Generate a wordcloud from the channel")
    async def wordcloud(self, ctx):
        # add emojis in excluded words
        excluded_words.extend([emoji.name for emoji in ctx.bot.emojis])     # TODO : can we move this elsewhere ?
        print("Wordcloud queried, fetching messages..")
        messages = await ctx.channel.history(limit=None).flatten()
        text = " ".join((map(lambda message: message.content.lower(), messages)))
        print("Generating wordcloud..")
        wordcloud = WordCloud(background_color='white', stopwords=excluded_words, max_words=50,
                              normalize_plurals=False).generate(text)
        plt.imshow(wordcloud)
        plt.axis("off")
        print("Wordcloud ready")
        plt.savefig(fname='wordcloud')
        await ctx.send(file=discord.File('wordcloud.png'))
        os.remove('wordcloud.png')


def setup(client):
    client.add_cog(WordcloudCommand(client))
