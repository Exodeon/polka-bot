from datetime import datetime
from pytz import timezone, all_timezones
from discord.ext import commands


class HourCommand(commands.Cog):
    def __init__(self, client):
        self.client = client

    @commands.command(help="Tells the hour in the given timezone (for example Europe/Paris or Pacific/Auckland)")
    async def hour(self, ctx, time_zone: str):
        zone = timezone(time_zone)
        time_in_zone = datetime.now(zone)
        time_readable = time_in_zone.strftime('%H:%S (%d/%m/%Y)')
        await ctx.message.channel.send(time_readable)

    @commands.command(help="List all timezones usable with hour command\n"
                           "⚠️ This is quite long and will send around 6 messages")
    async def hourlist(self, ctx):
        message = ""
        first_message = True
        for zone in all_timezones:
            if first_message:
                message += zone
                first_message = False
            else:
                message += f", {zone}"
            if len(message) > 1800:
                await ctx.message.channel.send(message)
                message = ""
                first_message = True
        await ctx.message.channel.send(message)


def setup(client):
    client.add_cog(HourCommand(client))
