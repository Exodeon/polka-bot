import os
from typing import Optional

import discord
from discord.ext import commands
import requests
from PIL import Image
import PIL.ImageOps


class InvertCommand(commands.Cog):
    def __init__(self, client):
        self.client = client

    @commands.command(help="Invert the colors of your avatar or the avatar of the given member")
    async def invert(self, ctx, member: Optional[discord.Member]):
        if not member:
            member = ctx.message.author
        with requests.get(member.display_avatar.url) as r:
            image_data = r.content
        with open('avatar.png', 'wb') as handler:
            handler.write(image_data)

        image = Image.open('avatar.png')
        if image.mode == 'RGBA':
            r, g, b, a = image.split()
            rgb_image = Image.merge('RGB', (r, g, b))

            inverted_image = PIL.ImageOps.invert(rgb_image)

            r2, g2, b2 = inverted_image.split()

            final_transparent_image = Image.merge('RGBA', (r2, g2, b2, a))

            final_transparent_image.save('avatar.png')

        else:
            inverted_image = PIL.ImageOps.invert(image)
            inverted_image.save('avatar.png')

        await ctx.message.channel.send(file=discord.File('avatar.png'))
        os.remove('avatar.png')





def setup(client):
    client.add_cog(InvertCommand(client))
