import os
from datetime import datetime, timedelta
from typing import Optional, Literal

import discord
from discord.ext import commands

import heapq

meme_channel_id = int(os.getenv("DEFAULT_STATS_CHANNEL_ID"))


class StatsCommand(commands.Cog):
    def __init__(self, client):
        self.client = client

    @commands.command(help="Show stats about messages only with custom emojis\n"
                      "Default channel is \"#memes-de-qualite\"\n"
                      "If no emoji is provided, it counts all reactions\n"
                      "If no time limit is provided, it gets all messages")
    async def stats(self, ctx, top: Optional[int] = 1, channel: Optional[discord.TextChannel] = None,
                    emoji: Optional[discord.Emoji] = None, time_limit: Optional[Literal["week", "month", "3-months", "6-months", "year"]] = None):
        after_date = None
        days_to_subtract = {
            "week": 7,
            "month": 30,
            "3-months": 90,
            "6-months": 180,
            "year": 365
        }
        if time_limit:
            after_date = datetime.now() - timedelta(days=days_to_subtract[time_limit])
        if emoji is None:
            if channel is None:
                channel = ctx.bot.get_channel(meme_channel_id)
            print("Stats queried, fetching messages..")
            messages = await channel.history(limit=None, after=after_date).flatten()
            print("Maximizing..")
            max_messages = heapq.nlargest(top, messages,
                                          key=lambda message: sum([react.count for react in message.reactions]))

            for position, max_message in enumerate(max_messages):
                await ctx.message.channel.send(content="**Top " + str(position + 1) + "**\n"
                                                                                      "Auteur·trice : " + str(
                    max_message.author) + "\n" + "Date : " + max_message.created_at.strftime("%d/%m/%Y") + "\n"
                                          "Réactions : " + str(
                    sum([react.count for react in max_message.reactions])) + "\n"
                                                       + max_message.content,
                                               files=[await file.to_file() for file in max_message.attachments])
            print("Stats sent")

        else:
            measure_emoji_name = emoji.name
            if channel is None:
                channel = ctx.bot.get_channel(meme_channel_id)
            print("Stats queried, fetching messages..")
            messages = await channel.history(limit=None, after=after_date).flatten()
            max_messages = heapq.nlargest(top, messages,
                                          key=lambda message: list(map(lambda reaction: next(
                                              (react for react in message.reactions if
                                               react.is_custom_emoji() and react.emoji.name == measure_emoji_name)).count
                                          if any(
                                              [react.is_custom_emoji() and react.emoji.name == measure_emoji_name for react
                                               in message.reactions]) else 0, message.reactions)))

            for position, max_message in enumerate(max_messages):
                await ctx.message.channel.send(content="**Top " + str(position + 1) + "**\n"
                                                                                      "Auteur·trice : " + str(
                    max_message.author) + "\n" + "Date : " + max_message.created_at.strftime("%d/%m/%Y") + "\n"
                                          "Réactions " + str(emoji) + " : " + str(next(
                    reaction for reaction in max_message.reactions if
                    reaction.is_custom_emoji() and reaction.emoji.name == measure_emoji_name).count) + "\n"
                                                       + max_message.content,
                                               files=[await file.to_file() for file in max_message.attachments])

            print("Stats sent")


def setup(client):
    client.add_cog(StatsCommand(client))
