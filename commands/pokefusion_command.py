import io

import aiohttp
import discord
from discord.ext import commands

pokefusion_url = "https://images.alexonsager.net/pokemon/fused/{0}/{0}.{1}.png"


class PokefusionCommand(commands.Cog):
    def __init__(self, client):
        self.client = client

    @commands.command(help="Shows a fusion of 2 Pokémon (Pokémon numbers from 0 to 151)")
    async def pokefusion(self, ctx, pokemon_number_a: int, pokemon_number_b: int):
        async with aiohttp.ClientSession() as session:
            result_image_url = pokefusion_url.format(pokemon_number_a, pokemon_number_b)
            async with session.get(result_image_url) as resp:
                if resp.status != 200:
                    print('Could not download file...')
                else:
                    data = io.BytesIO(await resp.read())
                    await ctx.message.channel.send(file=discord.File(data, 'pokefusion.png'))


def setup(client):
    client.add_cog(PokefusionCommand(client))
