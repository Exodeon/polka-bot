import requests
from discord.ext import commands

bored_api_url = "https://bored.api.lewagon.com/api/activity"


class BoredCommand(commands.Cog):
    def __init__(self, client):
        self.client = client

    @commands.command(help="Gives an idea of activity if you're bored")
    async def bored(self, ctx):
        response = requests.get(bored_api_url)
        await ctx.message.channel.send(response.json()["activity"])


def setup(client):
    client.add_cog(BoredCommand(client))
