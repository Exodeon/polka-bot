import io
import os
from random import choice
from time import sleep

import aiohttp
import discord
from discord.ext import commands
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials

# If modifying these scopes, delete the file token.json.
from googleapiclient.http import MediaIoBaseDownload
SCOPES = ['https://www.googleapis.com/auth/drive.readonly']
bibliobot_id = os.getenv("BIBLIOBOT_DRIVE_DIRECTORY_ID")


class GoogleDriveCommand(commands.Cog):
    def __init__(self, client):
        self.client = client
        self.service = None
        self.initialize_google_drive()

    def initialize_google_drive(self):
        creds = None
        # The file token.json stores the user's access and refresh tokens, and is
        # created automatically when the authorization flow completes for the first
        # time.
        if os.path.exists('token.json'):
            creds = Credentials.from_authorized_user_file('token.json', SCOPES)
        # If there are no (valid) credentials available, let the user log in.
        if not creds or not creds.valid:
            if creds and creds.expired and creds.refresh_token:
                creds.refresh(Request())
            else:
                flow = InstalledAppFlow.from_client_secrets_file(
                    'credentials.json', SCOPES)
                creds = flow.run_local_server(port=0)
            # Save the credentials for the next run
            with open('token.json', 'w') as token:
                token.write(creds.to_json())

        self.service = build('drive', 'v3', credentials=creds)

    async def get_file(self, name):
        try:
            results = self.service.files().list(q="fullText contains '{}' and '{}' in parents".format(name, bibliobot_id),
                                           spaces="drive",
                                           pageSize=10, fields="nextPageToken, files(id, name)").execute()
            items = results.get('files', [])
            if len(items) > 0:
                item = items[0]
                print(u'{0} ({1})'.format(item['name'], item['id']))
                file_id = item['id']
                request = self.service.files().get_media(fileId=file_id)
                fh = io.BytesIO()
                downloader = MediaIoBaseDownload(fh, request)
                done = False
                while done is False:
                    status, done = downloader.next_chunk()
                    print("Download %d%%." % int(status.progress() * 100))
                fh.seek(0)
                return item['name'], fh
            else:
                print("No result found, sending 404 image")
                images_404_urls = ["https://http.cat/404", "https://http.dog/404.jpg"]
                image_404_url = choice(images_404_urls)
                async with aiohttp.ClientSession() as session:
                    async with session.get(image_404_url) as resp:
                        if resp.status != 200:
                            print('Could not download file...')
                        else:
                            data = io.BytesIO(await resp.read())
                            return "404.jpg", data
        except HttpError as e:
            print("Error", e.resp.status, "with Google API")
            base_urls = ["https://http.cat/", "https://http.dog/"]
            image_url = choice(base_urls) + str(e.resp.status) + ".jpg"
            async with aiohttp.ClientSession() as session:
                async with session.get(image_url) as resp:
                    if resp.status != 200:
                        print('Could not download file...')
                    else:
                        data = io.BytesIO(await resp.read())
                        return str(e.resp.status) + ".png", data


    def get_audio(self, name):
        results = self.service.files().list(q="fullText contains '{}' and '{}' in parents".format(name, os.getenv("AUDIO_DIRECTORY_ID")),
                                       spaces="drive",
                                       pageSize=10, fields="nextPageToken, files(id, name)").execute()
        items = results.get('files', [])
        item = items[0]
        print(u'{0} ({1})'.format(item['name'], item['id']))
        file_id = item['id']
        request = self.service.files().get_media(fileId=file_id)
        fh = io.BytesIO()
        downloader = MediaIoBaseDownload(fh, request)
        done = False
        while done is False:
            status, done = downloader.next_chunk()
            print("Download %d%%." % int(status.progress() * 100))
        fh.seek(0)
        with open(item['name'], "wb") as f:
            f.write(fh.getbuffer())
        return item['name']

    @commands.command(help="Searchs an image / a video / a gif on the drive from the argument (title or word present in the image) and shows it")
    async def img(self, ctx, name):
        image_name, image_io = await self.get_file(name)
        await ctx.message.channel.send(file=discord.File(image_io, image_name))

    @commands.command(help="Search a sound or a music on the drive from the argument and play it")
    async def sound(self, ctx, name):
        if not ctx.message.author.voice:
            await ctx.send("{} is not connected to a voice channel".format(ctx.message.author.name))
            return
        else:
            channel = ctx.message.author.voice.channel
            vc = await channel.connect()
            audio = self.get_audio(name)
            vc.play(discord.FFmpegPCMAudio(source=audio))
            # Sleep while audio is playing.
            while vc.is_playing():
                sleep(1)
            os.remove(audio)
            await vc.disconnect()


def setup(client):
    client.add_cog(GoogleDriveCommand(client))
