import json

import requests
from discord.ext import commands

dicolink_api_key = "4_WcH_GGmv2VHAySrz0wX8N50YkAJTeq"

# only using the Larousse dictionary
dicolink_url = "https://api.dicolink.com/v1/mot/{word}/definitions?source=larousse&api_key={api_key}"


class DicoCommand(commands.Cog):
    def __init__(self, client):
        self.client = client

    @commands.command(help="Gives definitions of the word (test with Dicolink API)")
    async def dico(self, ctx, word: str):
        response = requests.get(dicolink_url.format(word=word, api_key=dicolink_api_key))
        if response.status_code != 200:
            await ctx.message.channel.send("Définition introuvable")
        else:
            message_to_send = ""
            for definition in response.json():
                # message_to_send += "- *" + definition["attributionText"] + "* : " + definition["definition"] + "\n"
                message_to_send += "- " + definition["definition"] + "\n"
            await ctx.message.channel.send(message_to_send)

def setup(client):
    client.add_cog(DicoCommand(client))
