import json

import discord
import requests
from discord.ext import commands

motivation_api_url = "https://zenquotes.io/api/random"


class MotivationCommand(commands.Cog):
    def __init__(self, client):
        self.client = client

    @commands.command(help="Gives a motivational quote")
    async def motivation(self, ctx):
        response = requests.get(motivation_api_url)
        response_json = json.loads(response.content.decode("utf-8"))[0]
        await ctx.message.channel.send(response_json["q"] + " *-" + response_json["a"] + "*")


def setup(client):
    client.add_cog(MotivationCommand(client))
