import os

import discord
from discord.ext import commands
import io
import aiohttp

cat_url = "https://thiscatdoesnotexist.com/"
cat_master_id = int(os.getenv("CAT_MASTER_ID"))


class CatCommand(commands.Cog):
    def __init__(self, client):
        self.client = client

    @commands.command(help="Shows a wonderful cat")
    async def catty(self, ctx):
        async with aiohttp.ClientSession() as session:
            async with session.get(cat_url) as resp:
                if resp.status != 200:
                    print('Could not download file...')
                else:
                    data = io.BytesIO(await resp.read())
                    if ctx.message.author.id == cat_master_id:
                        await ctx.message.channel.send("Oui maître", file=discord.File(data, 'cat.png'))
                    else:
                        await ctx.message.channel.send("Tiens {}".format(ctx.message.author.mention),
                                                       file=discord.File(data, 'cat.png'))


def setup(client):
    client.add_cog(CatCommand(client))
