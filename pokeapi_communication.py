import json

import requests

base_api_url = "https://pokeapi.co/api/v2/"


def get_all_types():
    api_response = requests.get(base_api_url + "type")

    # "unknown" and "shadow" are not really common types
    return [result["name"] for result in api_response.json()["results"]
                               if result["name"] != "unknown" and result["name"] != "shadow"]

def get_weakness_for_type(type: str):
    api_response = requests.get(base_api_url + "type/" + type)
    return [result["name"] for result in api_response.json()["damage_relations"]["double_damage_from"]]
